<?php
namespace Anytimestream\DAO;

class IndexType {
    const PRIMARY_KEY = "Primary Key";
    const TIMESTAMP = "Timestamp";
    const UNIQUE = "Unique";
    const FOREIGN_KEY = "Foreign Key";
}

