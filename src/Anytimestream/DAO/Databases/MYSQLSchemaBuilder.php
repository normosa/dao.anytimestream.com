<?php

/* ~ MYSQLSchemaBuilder.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - DAO                       |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\DAO\Databases;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\DataType;
use Anytimestream\DAO\PersistableObject;
use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Anytimestream Data Object
 * Generates Schema from PersistableObject for MYSQL Database
 * @author Norman Osaruyi
 * @package Anytimestream\DAO\Databases
 */
class MYSQLSchemaBuilder {

    private $dirs;
    private $excludes;
    private $schema;
    private static $instance;

    /**
     * Creates new Instance
     * @param Array $config connection info
     */
    private function __construct($dirs, $excludes) {
        $this->dirs = $dirs;
        $this->excludes = $excludes;
        $this->schema = "";
    }

    /**
     * Gets or create new Instance
     * @param Array $config connection info
     */
    public static function CreateBuilder($dirs, $excludes = array()) {
        if (self::$instance == null) {
            self::$instance = new MYSQLSchemaBuilder($dirs, $excludes);
        }
        return self::$instance;
    }

    public function build() {
        try {
            $this->setSchemaHeader();
            foreach ($this->dirs as $dir) {
                $this->processDirectory($dir);
            }
            $this->setSchemaFooter();
        } catch (Exception $e) {
            echo $e;
        }
    }

    /*
     * @return string
     */

    public function getSchema(): string {
        return $this->schema;
    }

    public function saveSchema(string $filename) {
        file_put_contents($filename, $this->schema);
    }

    private function processDirectory($dir) {
        $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST, RecursiveIteratorIterator::CATCH_GET_CHILD);
        foreach ($iterator as $path => $file) {
            if ($file->isFile() && !in_array($path, $this->excludes)) {
                $this->processFile($path);
            }
        }
    }

    private function processFile($file) {
        $namespace = $this->getNamespace($file);
        $classnames = $this->getClassNames($file);
        foreach ($classnames as $classname) {
            $fullClassname = $namespace . "\\$classname";
            $this->processClass($fullClassname);
        }
    }

    private function processClass($classname) {
        $obj = new $classname;
        if (is_subclass_of($obj, PersistableObject::class) && $obj->getTableAnnotation() != null) {
            $this->schema .= $this->getTableSchema($obj);
        }
    }

    private function setSchemaHeader() {
        $this->schema = "/*!40101 SET NAMES utf8 */;\n";
        $this->schema .= "\n/*!40101 SET SQL_MODE=''*/;\n";
        $this->schema .= "\n/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n";
        $this->schema .= "\n/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;\n";
    }

    private function setSchemaFooter() {
        $this->schema .= "\n/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;\n";
        $this->schema .= "\n/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n";
    }

    private function getTableSchema($obj) {
        //$database = $obj->getTableAnnotation()->database;
        $tablename = $obj->getTableAnnotation()->name;
        //$schema = "\nUSE `$database`;\n";
        $schema = "\nDROP TABLE IF EXISTS `$tablename`;\n";
        $schema .= "\nCREATE TABLE `$tablename` (\n";
        $schema .= $this->getColumnSchemas($obj);
        $schema .= $this->getPrimaryKeyColumnSchema($obj);
        $schema .= $this->getUniqueColumnSchemas($obj);
        $schema .= $this->getForeignKeyColumnSchemas($obj);
        $schema .= ") ENGINE=InnoDB DEFAULT CHARSET=latin1;\n";
        return str_replace(",\n)", "\n)", $schema);
    }

    private function getPrimaryKeyColumnSchema($obj) {
        $primary_key_column = $obj->getPrimaryKeyColumn();
        if ($primary_key_column != null) {
            return " PRIMARY KEY (`$primary_key_column`),\n";
        }
        return "";
    }

    private function getUniqueColumnSchemas($obj) {
        $schema = "";
        $unique_columns = $obj->getUniqueColumns();
        foreach ($unique_columns as $unique_column) {
            $unique_key_name = implode('_', $unique_column);
            $prefixColumnNames = preg_filter("/^/", "`", $unique_column);
            $unique_column_names = implode(',', preg_filter("/$/", "`", $prefixColumnNames));
            $schema .= " UNIQUE KEY `$unique_key_name`($unique_column_names),\n";
        }
        return $schema;
    }

    private function getForeignKeyColumnSchemas($obj) {
        $tablename = $obj->getTableAnnotation()->name;
        $schema = "";
        $foreign_keys = $obj->getForeignKeys();
        foreach ($foreign_keys as $foreign_key) {
            $key_column = $foreign_key->column;
            $key_name = $tablename . "_fk_" . $key_column;
            $table_name = $foreign_key->refTable;
            $column_name = $foreign_key->refColumn;
            $schema .= " KEY `$key_name` (`$key_column`),\n";
            $onUpdateCascade = ($foreign_key->onUpdateCascade) ? " ON UPDATE CASCADE" : "";
            $onDeleteCascade = ($foreign_key->onDeleteCascade) ? " ON DELETE CASCADE" : "";
            $schema .= " CONSTRAINT `$key_name` FOREIGN KEY (`$key_column`) REFERENCES `$table_name` (`$column_name`)$onUpdateCascade$onDeleteCascade,\n";
        }
        return $schema;
    }

    private function getColumnSchemas($obj) {
        $columns = $obj->getColumns();
        $column_schemas = array();
        $timestampColumn = $obj->getTimestampColumn();
        foreach ($columns as $column) {
            if (!$column->autoCreate) {
                $isTimestampColumn = $this->isTimestampColumn($timestampColumn, $column->name);
                $column_schemas[] = $this->getColumnSchema($column, $isTimestampColumn);
            }
        }
        return implode(',' . "\n", $column_schemas) . ',' . "\n";
    }

    private function isTimestampColumn(string $timestampColumn, string $column): bool {
        return ($timestampColumn != null && (strcmp($timestampColumn, $column) == 0));
    }

    private function getColumnSchema($column, $isTimestampColumn) {
        $allow_null = $column->allowNull ? "DEFAULT NULL" : "NOT NULL";
        $data_type_schema = $this->getColumnDataTypeSchema($column);
        $schema = " `$column->name` $data_type_schema $allow_null";
        $schema .= $this->getColumnDefault($column, $isTimestampColumn);
        return $schema;
    }

    private function getColumnDefault(Column $column, string $isTimestampColumn): string {
        if ($isTimestampColumn) {
            return " DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
        } else if (isset($column->extra['default'])) {
            return " DEFAULT " . $column->extra['default'];
        } else if (isset($column->extra['extras'])) {
            return " " . $column->extra['extras'];
        }
        return "";
    }

    private function getColumnDataTypeSchema($column) {
        $data_type_schema = "";
        switch ($column->dataType) {
            case DataType::STRING:
                $data_type_schema = "varchar(" . $column->extra['max'] . ")";
                break;
            case DataType::BOOLEAN:
                $data_type_schema = "tinyint(1)";
                break;
            case DataType::TINYINT:
                $data_type_schema = "tinyint(" . $column->extra['max'] . ")";
                break;
            case DataType::INTEGER:
                $data_type_schema = "int(11)";
                break;
            case DataType::DOUBLE:
                $data_type_schema = "double";
                break;
            case DataType::DECIMAL:
                $data_type_schema = "decimal(" . $column->extra['max'] . ")";
                break;
            case DataType::MEDIUMTEXT:
                $data_type_schema = "mediumtext";
                break;
            case DataType::DATETIME:
                $data_type_schema = "datetime";
                break;
            case DataType::DATE:
                $data_type_schema = "date";
                break;
            case DataType::TIME:
                $data_type_schema = "time";
                break;
            case DataType::TIMESTAMP:
                $data_type_schema = "timestamp";
                break;
            case DataType::JSON:
                $data_type_schema = "json";
                break;
            default:
                throw new Exception("DataType Not Found: $column->dataType");
        }
        return $data_type_schema;
    }

    private function getNamespace(string $file) {
        $tokens = $this->getTokens($file);
        $count = count($tokens);
        $i = 0;
        $namespace = '';
        $namespace_ok = false;
        while ($i < $count) {
            $token = $tokens[$i];
            if (is_array($token) && $token[0] === T_NAMESPACE) {
                $this->extractNamespace($namespace, $namespace_ok, $i, $count, $tokens);
                break;
            }
            $i++;
        }
        return $namespace;
    }

    private function extractNamespace(string &$namespace, bool &$namespace_ok, int &$i, int $count, array $tokens) {
        while (++$i < $count) {
            if ($tokens[$i] === ';') {
                $namespace_ok = true;
                $namespace = trim($namespace);
                break;
            }
            $namespace .= is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i];
        }
    }

    private function getClassNames($file) {
        $classnames = array();
        $tokens = $this->getTokens($file);
        $class_token = false;
        foreach ($tokens as $token) {
            if (is_array($token) && $token[0] == T_CLASS) {
                $class_token = true;
            } else if (is_array($token) && $class_token && $token[0] == T_STRING) {
                $classnames[] = $token[1];
                $class_token = false;
                break;
            }
        }
        return $classnames;
    }

    private function getTokens(string $file): array {
        $php_file = file_get_contents($file);
        return token_get_all($php_file);
    }

}
