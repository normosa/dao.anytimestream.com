<?php
namespace Anytimestream\DAO\Databases;

use Anytimestream\DAO\PersistableObject;

interface Database {
    
    /**
     * creates Database Instance from configuration
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return Database Database object
     */
    public static function GetInstance(Array $config): Database;
    
    /**
     * finds PersistableObjects from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return PersistableObject instance
     */
    public function find(string $classname, Array $filter): PersistableObject;
    
    /**
     * find PersistableObjects from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @param int $index starting index
     * @param int $size maximum records
     * @param string $orderBy order by
     * @param string $orderByDir order by direction
     * @return Array PersistableObjects
     */
    public function findAll(string $classname, Array $filter = Array(), int $index = 0, int $size = PHP_INT_MAX, string $orderBy = null, string $orderByDir = "ASC"): Array;
    
    /**
     * deletes PersistableObject from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return bool true on success, false on failure
     */
    public function delete(string $classname, Array $filter = Array());
    
    /**
     * delete PersistableObjects from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return int No of PersistableObjects deleted
     */
    public function deleteAll(string $classname, Array $filter = Array()): int;
    
    /**
     * Query a database
     * @param string $classname PersistableObject class name
     * @param string $query query string
     * @param Array $columnValues values to match against placeholder
     * @param int $index starting index
     * @param int $size maximum records
     * @param string $orderBy order by
     * @param string $orderByDir order by direction
     * @return Array PersistableObjects
     */
    public function query(string $classname, string $query, Array $columnValues, int $index = 0, int $size = PHP_INT_MAX, string $orderBy = null, string $orderByDir = "ASC"): Array;
    
    /**
     * Saves PersistableObject to database
     * @param PersistableObject $persistableObject object to save
     * @return bool true on successful, false on failure
     */
    public function save(PersistableObject $persistableObject);
    
    /**
     * Close connection
     */
    public function close();
    
    /**
     * Begin system wide transaction, subsequent calls to beginTransaction
     */
    public function beginUniversalTransaction();
    
    /**
     * Begin transaction when applicable
     */
    public function beginTransaction();
    
    /**
     * Commit transaction when applicable
     */
    public function commit();
    
    /**
     * Commit system wide transactions, ignores calls to commit
     */
    public function commitUniversal();
    
    /**
     * RollBack Transaction when applicable
     */
    public function rollBack();
}

