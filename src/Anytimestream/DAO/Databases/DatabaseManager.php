<?php
/*~ DatabaseManager.php
.------------------------------------------------------------------------------.
|  Software: Anytimestream Technologies Limited - DAO                          |
|   Version: 1.0.0                                                             |
|      Site: https://code.google.com/a/apache-extras.org/p/phpmailer/          |
| -----------------------------------------------------------------------------|
|     Admin: Norman Osaruyi (project admininistrator)                          |
|   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                   |
|   Founder: Anytimestream Technologies Limited                                |
| Copyright (c) 2018, Anytimestream Technologies Limited. All Rights Reserved. |
| -----------------------------------------------------------------------------|
|   License: Distributed under the Lesser General Public License (LGPL)        |
|            http://www.gnu.org/copyleft/lesser.html                           |
| This program is distributed in the hope that it will be useful - WITHOUT     |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        |
| FITNESS FOR A PARTICULAR PURPOSE.                                            |
'------------------------------------------------------------------------------'
*/
namespace Anytimestream\DAO\Databases;

use Anytimestream\DAO\Databases\MYSQLDatabase;

/**
 * Anytimestream Data Object
 * Performs operations on MYSQL Database
 * @author Norman Osaruyi
 * @package Anytimestream\DAO\Databases
 */
class DatabaseManager {

    public static function GetDatabase(): Database {
        //move datasource selection into metadata file
        $mysqlConfig['host'] = DB_HOST;
        $mysqlConfig['username'] = DB_USER;
        $mysqlConfig['password'] = DB_PASSWORD;
        $mysqlConfig['dbname'] = DB_NAME;
        
        return MYSQLDatabase::GetInstance($mysqlConfig);
    }
    
    public static function GetNoDBDatabase(): Database {
        //move datasource selection into metadata file
        $mysqlConfig['host'] = DB_HOST;
        $mysqlConfig['username'] = DB_USER;
        $mysqlConfig['password'] = DB_PASSWORD;
        
        return MYSQLDatabase::GetInstance($mysqlConfig);
    }

}
