<?php
/*~ MYSQLDatabase.php
.------------------------------------------------------------------------------.
|  Software: Anytimestream Technologies Limited - DAO                          |
|   Version: 1.0.0                                                             |
|      Site: https://code.google.com/a/apache-extras.org/p/phpmailer/          |
| -----------------------------------------------------------------------------|
|     Admin: Norman Osaruyi (project admininistrator)                          |
|   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                   |
|   Founder: Anytimestream Technologies Limited                                |
| Copyright (c) 2018, Anytimestream Technologies Limited. All Rights Reserved. |
| -----------------------------------------------------------------------------|
|   License: Distributed under the Lesser General Public License (LGPL)        |
|            http://www.gnu.org/copyleft/lesser.html                           |
| This program is distributed in the hope that it will be useful - WITHOUT     |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        |
| FITNESS FOR A PARTICULAR PURPOSE.                                            |
'------------------------------------------------------------------------------'
*/
namespace Anytimestream\DAO\Databases;

use Anytimestream\DAO\Databases\Exception\ConnectionException;
use Anytimestream\DAO\Databases\Exception\ObjectDeleteException;
use Anytimestream\DAO\Databases\Exception\ObjectInsertException;
use Anytimestream\DAO\Databases\Exception\ObjectNotFoundException;
use Anytimestream\DAO\Databases\Exception\ObjectUpdateException;
use Anytimestream\DAO\PersistableObject;
use Exception;
use PDO;

/**
 * Anytimestream Data Object
 * Performs operations on MYSQL Database
 * @author Norman Osaruyi
 * @package Anytimestream\DAO\Databases
 */
class MYSQLDatabase implements Database {

    private $connection;
    private static $instance;
    private $isUniversalTransaction;

    /**
     * Creates new Instance
     * @param Array $config connection info
     */
    private function __construct(Array $config) {
        try {
            $dbURL = "mysql:host={$config['host']}";
            if(isset($config['dbname'])){
                $dbURL .= ";dbname={$config['dbname']}";
            }
            $this->connection = new PDO($dbURL, $config['username'], $config['password']);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->isUniversalTransaction = false;
        } catch (Exception $e) {
            throw new ConnectionException();
        }
    }

    /**
     * creates Database Instance from configuration
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return Database Database object
     */
    public static function GetInstance(Array $config): Database {
        if (self::$instance == null) {
            self::$instance = new MYSQLDatabase($config);
        }
        return self::$instance;
    }
    
    /**
     * Begin system wide transaction, subsequent calls to beginTransaction
     */
    public function beginUniversalTransaction() {
        if (!$this->connection->inTransaction()) {
            $this->connection->beginTransaction();
            $this->isUniversalTransaction = true;
        }
    }

    /**
     * Begin transaction when applicable
     */
    public function beginTransaction() {
        if (!$this->isUniversalTransaction && !$this->connection->inTransaction()) {
            $this->connection->beginTransaction();
        }
    }

    /**
     * Commit transaction when applicable
     */
    public function commit() {
        if (!$this->isUniversalTransaction && $this->connection->inTransaction()) {
            $this->connection->commit();
        }
    }
    
    /**
     * Commit system wide transactions, ignores calls to commit
     */
    public function commitUniversal() {
        if ($this->isUniversalTransaction && $this->connection->inTransaction()) {
            $this->connection->commit();
        }
    }

    /**
     * Close connection
     */
    public function close() {
        $this->connection = null;
        self::$instance = null;
    }

    /**
     * RollBack Transaction when applicable
     */
    public function rollBack() {
        if ($this->connection->inTransaction()) {
            $this->connection->rollBack();
        }
    }
    
    /**
     * finds PersistableObjects from Database by Primary Key
     * @param String $classname PersistableObject class name
     * @param String $id primary key value
     * @return PersistableObject instance
     */
    public function findById($classname, $id) {
        $primaryKeyColumn = $this->getPrimaryKeyColumn($classname);
        return $this->find($classname, array($primaryKeyColumn => $id));
    }

    /**
     * finds PersistableObjects from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return PersistableObject instance
     */
    public function find(string $classname, Array $filter): PersistableObject {
        $persistableObjects = $this->findAll($classname, $filter);
        if (count($persistableObjects) > 0) {
            return $persistableObjects[0];
        }
        throw new ObjectNotFoundException();
    }

    /**
     * find PersistableObjects from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @param int $index starting index
     * @param int $size maximum records
     * @param string $orderBy order by
     * @param string $orderByDir order by direction
     * @return Array PersistableObjects
     */
    public function findAll(string $classname, Array $filter = Array(), int $index = 0, int $size = PHP_INT_MAX, string $orderBy = null, string $orderByDir = "ASC"): Array {
        $persistableObject = new $classname;
        $dataSourceName = $persistableObject->getDataSourceName();
        $columnValues = array_values($filter);
        $strWhereClause = $this->getWhereClause($persistableObject, $filter);
        $query = "select * from $dataSourceName $strWhereClause";
        return $this->query($classname, $query, $columnValues, $index, $size, $orderBy, $orderByDir);
    }
    
    /**
     * deletes PersistableObject from Database by Primary Key
     * @param String $classname PersistableObject class name
     * @param String $id primary key value
     * @return bool true on success, false on failure
     */
    public function deleteById(string $classname, string $id): bool {
        $primaryKeyColumn = $this->getPrimaryKeyColumn($classname);
        return $this->delete($classname, array($primaryKeyColumn => $id));
    }

    /**
     * deletes PersistableObject from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return bool true on success, false on failure
     */
    public function delete(string $classname, Array $filter = Array()): bool {
        $rowCount = $this->deleteAll($classname, $filter);
        if ($rowCount > 0) {
            return true;
        }
        throw new ObjectDeleteException();
    }

    /**
     * delete PersistableObjects from Database
     * @param String $classname PersistableObject class name
     * @param Array $filter properties/values to search
     * @return int No of PersistableObjects deleted
     */
    public function deleteAll(string $classname, Array $filter = Array()): int {
        $persistableObject = new $classname;
        $dataSourceName = $persistableObject->getDataSourceName();
        $columnValues = array_values($filter);
        $strWhereClause = $this->getWhereClause($persistableObject, $filter);
        $query = "delete from $dataSourceName $strWhereClause";
        $statement = $this->executeStatment($query, $columnValues);
        return $statement->rowCount();
    }

    /**
     * Query a database
     * @param string $classname PersistableObject class name
     * @param string $query query string
     * @param Array $columnValues values to match against placeholder
     * @param int $index starting index
     * @param int $size maximum records
     * @param string $orderBy order by
     * @param string $orderByDir order by direction
     * @return Array PersistableObjects
     */
    public function query(string $classname, string $query, Array $columnValues, int $index = 0, int $size = PHP_INT_MAX, string $orderBy = null, string $orderByDir = "ASC"): Array {
        $persistableObjects = array();
        $strOrderBy = ($orderBy != null) ? "order by $orderBy $orderByDir" : "";
        $sql = "$query $strOrderBy limit $index, $size";
        $statement = $this->executeStatment($sql, $columnValues, $index, $size, $orderBy, $orderByDir);
        while (($row = $statement->fetch())) {
            $object = new $classname();
            $object->setRowData($row);
            $persistableObjects[] = $object;
        }
        return $persistableObjects;
    }

    /**
     * Executes sql to database
     * @param String $sql Query string
     * @param String $query query string
     * @param Array $columnValues values to bind
     * @return Statement result statement
     */
    private function executeStatment($sql, $columnValues) {
        $statement = $this->connection->prepare($sql);
        $statement->execute($columnValues);
        return $statement;
    }

    /**
     * Extracts where clause from filter
     * @param PersistableObject $persistableObject source object
     * @param Array $filter property/values
     * @return String where clause
     */
    private function getWhereClause($persistableObject, $filter) {
        $filterCount = count($filter);
        $columnNames = ($filterCount > 0) ? $persistableObject->getColumnNames(array_keys($filter)) : array();
        $columnValues = array_values($filter);
        $quotedColumnNames = $this->getQuotedColumnNames($columnNames);
        return ($filterCount > 0) ? " where " . $this->getFilter($quotedColumnNames, $columnValues) : "";
    }

    /**
     * Saves PersistableObject to database
     * @param PersistableObject $persistableObject object to save
     * @return bool true on successful, false on failure
     */
    public function save(PersistableObject $persistableObject) {
        return ($persistableObject->isNew()) ? $this->insert($persistableObject) : ($persistableObject->isDirty())? $this->update($persistableObject): 0;
    }

    /**
     * Performs SQL insert to database
     * @param PersistableObject $persistableObject object to persist
     * @return int row changes
     */
    private function insert(PersistableObject $persistableObject) {
        $dataSourceName = $persistableObject->getDataSourceName();
        $rowData = $persistableObject->getRowData();
        $columnNames = array_keys($rowData);
        $columnValues = array_values($rowData);
        $strColumns = implode(',', $this->getQuotedColumnNames($columnNames));
        $columnPlaceholders = $this->getColumnPlaceholders($columnNames);
        $strPlaceholders = implode(',', $columnPlaceholders);
        $sql = "insert into $dataSourceName ($strColumns) values($strPlaceholders)";
        $placeholderValues = array_combine($columnPlaceholders, $columnValues);
        $statement = $this->executeStatment($sql, $placeholderValues);
        $rowCount = $statement->rowCount();
        if ($rowCount < 1) {
            throw new ObjectInsertException();
        }
    }

    /**
     * Performs SQL update to database
     * @param PersistableObject $persistableObject object to persist
     * @return int row changes
     */
    private function update(PersistableObject $persistableObject) {
        $dataSourceName = $persistableObject->getDataSourceName();
        $rowData = $persistableObject->getRowData();
        $primaryKeyColumn = $persistableObject->getPrimaryKeyColumn();
        $primaryKeyColumnValue = $rowData[$primaryKeyColumn];
        unset($rowData[$primaryKeyColumn]);
        $columnNames = array_keys($rowData);
        $columnValues = array_values($rowData);
        $columnValues[] = $primaryKeyColumnValue;
        $timestampColumn = $persistableObject->getTimestampColumn();
        if ($timestampColumn != null) {
            $columnValues[] = $persistableObject->getColumnValue($timestampColumn);
        }
        $strColumns = implode(' = ?,', $this->getQuotedColumnNames($columnNames)) . " = ?";
        $timestampCheck = ($timestampColumn == null) ? "" : "and `$timestampColumn` = ?";
        $sql = "update $dataSourceName set $strColumns where `$primaryKeyColumn` = ? $timestampCheck";
        $rowCount = $this->executeStatment($sql, $columnValues)->rowCount();
        if ($rowCount < 1) {
            throw new ObjectUpdateException();
        }
    }

    /**
     * Add single quote to column names
     * @param Array $columnNames column names
     * @return array quoted column names
     */
    private function getQuotedColumnNames(Array $columnNames): Array {
        $prefixColumnNames = preg_filter("/^/", "`", $columnNames);
        return preg_filter("/$/", "`", $prefixColumnNames);
    }

    /**
     * Add colon to column names
     * @param Array $columnNames column names
     * @return array placeholders
     */
    private function getColumnPlaceholders(Array $columnNames): Array {
        return preg_filter("/^/", ":", $columnNames);
    }

    /**
     * Extract filters
     * @param Array $quotedColumnNames quoted column names
     * @param Array $columnValues column values
     * @return String filter
     */
    private function getFilter(Array $quotedColumnNames, Array &$columnValues): string {
        $strWhereClause = "";
        for ($i = 0; $i < count($quotedColumnNames); $i++) {
            $strWhereClause .= ($i > 0) ? " and " : "";
            if (is_null($columnValues[$i])) {
                $strWhereClause .= $quotedColumnNames[$i] . " IS NULL";
                unset($columnValues[$i]);
            } else {
                $strWhereClause .= $quotedColumnNames[$i] . " = ?";
            }
        }
        return $strWhereClause;
    }
    
    /**
     * Gets primary key from classname
     * @param string $classname name of class
     * @return string primary key
     */
    private function getPrimaryKeyColumn(string $classname): string {
        $persistableObject = new $classname;
        return $persistableObject->getPrimaryKeyColumn();
    }

}
