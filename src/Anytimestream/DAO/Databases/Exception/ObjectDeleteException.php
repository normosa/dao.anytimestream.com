<?php
/*~ DatabaseConnectionException.php
.------------------------------------------------------------------------------.
|  Software: Anytimestream Technologies Limited - DAO                          |
|   Version: 1.0.0                                                             |
|      Site: https://code.google.com/a/apache-extras.org/p/phpmailer/          |
| -----------------------------------------------------------------------------|
|     Admin: Norman Osaruyi (project admininistrator)                          |
|   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                   |
|   Founder: Anytimestream Technologies Limited                                |
| Copyright (c) 2018, Anytimestream Technologies Limited. All Rights Reserved. |
| -----------------------------------------------------------------------------|
|   License: Distributed under the Lesser General Public License (LGPL)        |
|            http://www.gnu.org/copyleft/lesser.html                           |
| This program is distributed in the hope that it will be useful - WITHOUT     |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        |
| FITNESS FOR A PARTICULAR PURPOSE.                                            |
'------------------------------------------------------------------------------'
*/
namespace Anytimestream\DAO\Databases\Exception;

use Exception;

class ObjectDeleteException extends Exception {
    
}