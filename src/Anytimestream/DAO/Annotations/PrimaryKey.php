<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"CLASS"}) 
 */
class PrimaryKey {
    
    /** @Required */
    public $column;
}

