<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"CLASS"}) 
 */
class Timestamp {
    
    /** @Required */
    public $column;
}

