<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"PROPERTY"}) 
 */
class Column {
    
    /** @Required */
    public $name;
    
    /** @Required */
    public $dataType;
    
    public $allowNull = false;
    
    public $isSavable = true;
    
    public $extra = array();
    
    public $autoCreate = false;
}

