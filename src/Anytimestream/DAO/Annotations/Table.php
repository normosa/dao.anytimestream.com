<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"CLASS"}) 
 */
class Table {
    
    /** @Required */
    public $name;
    
    public $database;
}

