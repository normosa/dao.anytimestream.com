<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"CLASS"}) 
 */
class Index {
    
    /** @Required */
    public $type;
    
    /** @Required */
    public $columns;
    
    public $references;
}

