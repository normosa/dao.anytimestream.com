<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"CLASS"}) 
 */
class ForeignKey {
    
    /** @Required */
    public $column;
    
    /** @Required */
    public $refTable;
    
    /** @Required */
    public $refColumn;
    
    public $onUpdateCascade = true;
    
    public $onDeleteCascade = true;
}

