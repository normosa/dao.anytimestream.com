<?php
namespace Anytimestream\DAO\Annotations;

/** 
 * @Annotation
 * @Target({"PROPERTY"}) 
 */
class Validator {
    
    /** @Required */
    public $validationRule;
    
    /** @Required */
    public $errorCode;
    
    /** @Required */
    public $extra;
    
    public function validate($value){
        return (new $this->validationRule($this->extra))->validate($value);
    }
}

