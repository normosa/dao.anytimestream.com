<?php
namespace Anytimestream\DAO;

use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\DataType;

abstract class SoftDeletePersistableObject extends PersistableObject{
    
    /**
     * @Column(name="deleted", dataType=DataType::INTEGER)
     */
    protected $deleted;
    
    /**
     * @Column(name="deleted_timestamp", dataType=DataType::DATETIME, allowNull=true)
     */
    protected $deletedTimestamp;
    
    public function __construct() {
        parent::__construct();
        $this->deleted = 0;
        $this->deletedTimestamp = "1753-01-01 00:00:00";
    }
    
    public function getDeleted(){
        return $this->deleted;
    }
    
    public function setDeleted(){
        $this->setProperty("deleted", 1);
        $this->setProperty("deletedTimestamp", date('Y-m-d H:i:s'));
    }
    
    public function getDeletedTimestamp(){
        return $this->deletedTimestamp;
    }
}
