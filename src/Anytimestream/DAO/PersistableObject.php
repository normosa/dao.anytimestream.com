<?php

namespace Anytimestream\DAO;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\ForeignKey;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use ReflectionClass;
use ReflectionProperty;

abstract class PersistableObject {
    
    private $annotationReader;
    private $reflectionClass;
    protected $validationErrors;
    private $classAnnotations;
    private $isNew;
    private $isDirty;
    
    public function __construct() {
        $this->propertyAnnotations = array();
        $this->isNew = true;
        $this->isDirty = false;
        AnnotationRegistry::registerAutoloadNamespace("Anytimestream\DAO\Annotations", $this->getBaseDir());
    }
    
    private function getBaseDir(): string {
        return substr(__DIR__, 0, strlen(__DIR__) - strlen(__NAMESPACE__));
    }

    public function getTableAnnotation() {
        return $this->getAnnotationReader()->getClassAnnotation($this->getReflectionClass(), 'Anytimestream\DAO\Annotations\Table');
    }
    
    public function getDataSourceName(){
        return $this->getTableAnnotation()->name;
    }
    
    public function getIndexAnnotations($indexType = null) {
        $indexes = array();
        foreach ($this->getClassAnnotations() AS $annotation) {
            if (($annotation instanceof Index && $indexType == null) || ($indexType != null && $annotation instanceof Index && $annotation->type == $indexType)) {
                $indexes[] = $annotation;
            }
        }
        return $indexes;
    }
    
    public function getPrimaryKeyColumn(){
        foreach ($this->getClassAnnotations() AS $annotation) {
            if ($annotation instanceof PrimaryKey) {
                return $annotation->column;
            }
        }
        return null;
    }
    
    public function getTimestampColumn(){
        foreach ($this->getClassAnnotations() AS $annotation) {
            if ($annotation instanceof Timestamp) {
                return $annotation->column;
            }
        }
        return null;
    }
    
    private function getIndexColumn($indexType){
        foreach ($this->getIndexAnnotations() AS $index_annotation) {
            if ($index_annotation->type == $indexType && count($index_annotation->columns) > 0) {
                return $index_annotation->columns[0];
            }
        }
        return null;
    }
    
    public function getUniqueColumns(){
        return $this->getIndexColumns(IndexType::UNIQUE);
    }
    
    public function getForeignKeys(){
        $foreignKeys = array();
        foreach ($this->getClassAnnotations() AS $annotation) {
            if ($annotation instanceof ForeignKey) {
                $foreignKeys[] = $annotation;
            }
        }
        return $foreignKeys;
    }
    
    public function getIndexColumns($indexType){
        $columns = array();
        foreach ($this->getIndexAnnotations() AS $index_annotation) {
            if ($index_annotation->type == $indexType && count($index_annotation->columns) > 0) {
                $columns[] = $index_annotation->columns;
            }
        }
        return $columns;
    }
    
    private function getAnnotationReader(){
        if($this->annotationReader == null){
            $this->annotationReader = new AnnotationReader();
        }
        return $this->annotationReader;
    }
    
    public function getReflectionClass(){
        if($this->reflectionClass == null){
            $this->reflectionClass = new ReflectionClass($this);
        }
        return $this->reflectionClass;
    }
    
    public function getReflectionProperty($property){
        return new ReflectionProperty(get_class($this), $property);
    }
    
    private function getClassAnnotations(){
        if($this->classAnnotations == null){
            $this->classAnnotations = $this->getAnnotationReader()->getClassAnnotations($this->getReflectionClass());
        }
        return $this->classAnnotations;
    }
    
    public function getColumnAnnotation($property){
        $reflection_property = $this->getReflectionProperty($property);
        return $this->getAnnotationReader()->getPropertyAnnotation($reflection_property, 'Anytimestream\DAO\Annotations\Column');
    }
    
    public function getValidatorAnnotations($property){
        $validatorAnnotations = array();
        $reflection_property = $this->getReflectionProperty($property);
        $propertyAnnotations = $this->getAnnotationReader()->getPropertyAnnotations($reflection_property);
        foreach ($propertyAnnotations as $propertyAnnotation) {
            if($propertyAnnotation instanceof Validator){
                $validatorAnnotations[] = $propertyAnnotation;
            }
        }
        return $validatorAnnotations;
    }
    
    public function validate(): bool {
        $this->validationErrors = array();
        $properties = $this->getReflectionClass()->getProperties();
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $validators = $this->getValidatorAnnotations($property->getName());
            foreach($validators as $validator){
                $result = $validator->validate($property->getValue($this));
                if(!$result){
                    $this->validationErrors[$property->getName()][] = $validator->errorCode;
                }
            }
        }
        return (count($this->validationErrors) == 0)? true: false;
    }
    
    /**
     * Retrieves Validation Errors
     * @return Array containing errors indexed by property
     */
    public function getValidationErrors() {
        return $this->validationErrors;
    }
    
    public function isNew(){
        return $this->isNew;
    }
    
    public function isDirty(){
        return $this->isDirty;
    }
    
    /**
     * Extract column names
     * @param Array $propertyNames property names
     * @return Array column names
     */
    public function getColumnNames($propertyNames) {
        $columnNames = array();
        foreach ($propertyNames as $propertyName) {
            $property = $this->getReflectionClass()->getProperty($propertyName);
            $column = $this->getColumnAnnotation($property->getName());
            if ($column != null) {
                $columnNames[] = $column->name;
            }
        }
        return $columnNames;
    }
    
    /**
     * Extract column value
     * @param String $columnName column name
     * @return String column value
     */
    public function getColumnValue($columnName) {
        $properties = $this->getReflectionClass()->getProperties();
        foreach ($properties as $property) {
            $column = $this->getColumnAnnotation($property->getName());
            if ($column != null && $column->name === $columnName) {
                $property->setAccessible(true);
                return $property->getValue($this);
            }
        }
        return null;
    }
    
    /**
     * Extract columns
     * @return Array columns
     */
    public function getColumns() {
        $columns = array();
        $properties = $this->getReflectionClass()->getProperties();
        foreach ($properties as $property) {
            $column = $this->getColumnAnnotation($property->getName());
            if ($column != null) {
                $columns[] = $column;
            }
        }
        return $columns;
    }
    
    /**
     * Extract row data from PersistableObject
     * @return Array row containing column values indexed by column names
     */
    public function getRowData() {
        $rowData = array();
        $properties = $this->getReflectionClass()->getProperties();
        foreach ($properties as $property) {
            $column = $this->getColumnAnnotation($property->getName());
            $property->setAccessible(true);
            $value = $property->getValue($this);
            if ($column != null && $column->isSavable && isset($value)) {
                $rowData[$column->name] = $value;
            }
        }
        return $rowData;
    }
    
    /**
     * Load row data that into PersistableObject
     * @param Array $rowData row from DataSource
     */
    public function setRowData($rowData) {
        $properties = $this->getReflectionClass()->getProperties();
        foreach ($properties as $property) {
            $column = $this->getColumnAnnotation($property->getName());
            $property->setAccessible(true);
            if ($column != null && isset($rowData[$column->name])) {
                $property->setValue($this, $rowData[$column->name]);
            }
        }
        $this->markOld();
    }
    
    public function setProperty($property, $value){
        $reflectionProperty = $this->getReflectionProperty($property);
        $reflectionProperty->setAccessible(true);
        if($reflectionProperty->getValue($this) != $value){
            $reflectionProperty->setValue($this, $value);
            $this->isDirty = true;
        }
        return $this;
    }
    
    /**
     * Change object state to an existing record
     */
    public function markOld(){
        $this->isDirty = false;
        $this->isNew = false;
    }
}
