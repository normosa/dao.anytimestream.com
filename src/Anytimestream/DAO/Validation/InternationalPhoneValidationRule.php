<?php
/*~ InternationalPhoneValidationRule.php
.---------------------------------------------------------------------------.
|  Software: Anytimestream Technologies Limited - DAO                       |
| ------------------------------------------------------------------------- |
|     Admin: Norman Osaruyi (project admininistrator)                       |
|   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
|   Founder: Anytimestream Technologies Limited                             |
| Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
| ------------------------------------------------------------------------- |
|   License: Distributed under the Lesser General Public License (LGPL)     |
|            http://www.gnu.org/copyleft/lesser.html                        |
| This program is distributed in the hope that it will be useful - WITHOUT  |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
| FITNESS FOR A PARTICULAR PURPOSE.                                         |
'---------------------------------------------------------------------------'
*/
namespace Anytimestream\DAO\Validation;

class InternationalPhoneValidationRule implements ValidationRule {

    private $allowNull = false;

    /**
     * Creates new Instance
     * @param Array $extra optional settings
     */
    public function __construct($extra = null) {
        if(isset($extra) && isset($extra['allowNull'])){
            $this->allowNull = $extra['allowNull'];
        }
    }

    /**
     * Validates Method
     * @param mixed $value to validate
     * @return bool true or false
     */
    public function validate($value): bool {
        $strLen = strlen($value);
        $isInRange = $strLen >= 8 && $strLen <= 15;
        return (($this->allowNull && strlen($value) == 0) || (is_numeric($value) && !is_float($value) && $isInRange));
    }

}
