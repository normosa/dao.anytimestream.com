<?php
/*~ ValidationRule.php
.---------------------------------------------------------------------------.
|  Software: Anytimestream Technologies Limited - DAO                       |
| ------------------------------------------------------------------------- |
|     Admin: Norman Osaruyi (project admininistrator)                       |
|   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
|   Founder: Anytimestream Technologies Limited                             |
| Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
| ------------------------------------------------------------------------- |
|   License: Distributed under the Lesser General Public License (LGPL)     |
|            http://www.gnu.org/copyleft/lesser.html                        |
| This program is distributed in the hope that it will be useful - WITHOUT  |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
| FITNESS FOR A PARTICULAR PURPOSE.                                         |
'---------------------------------------------------------------------------'
*/
namespace Anytimestream\Dao\Validation;

interface ValidationRule {
    
    /**
     * Creates new Instance
     * @param Array $extra optional settings
     */
    public function __construct($extra = null);
    
    /**
     * Validates Method
     * @param mixed $value to validate
     * @return bool true or false
     */
    public function validate($value): bool;
}