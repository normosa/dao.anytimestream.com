<?php
/*~ DateTimeValidationRule.php
.---------------------------------------------------------------------------.
|  Software: Anytimestream Technologies Limited - DAO                       |
| ------------------------------------------------------------------------- |
|     Admin: Norman Osaruyi (project admininistrator)                       |
|   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
|   Founder: Anytimestream Technologies Limited                             |
| Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
| ------------------------------------------------------------------------- |
|   License: Distributed under the Lesser General Public License (LGPL)     |
|            http://www.gnu.org/copyleft/lesser.html                        |
| This program is distributed in the hope that it will be useful - WITHOUT  |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
| FITNESS FOR A PARTICULAR PURPOSE.                                         |
'---------------------------------------------------------------------------'
*/
namespace Anytimestream\DAO\Validation;

class DateTimeValidationRule implements ValidationRule {

    private $dateValidationRule;
    private $timeValidationRule;
    private $allowNull = false;

    /**
     * Creates new Instance
     * @param Array $extra optional settings
     */
    public function __construct($extra = null) {
        $this->dateValidationRule = new DateValidationRule($extra);
        $this->timeValidationRule = new TimeValidationRule($extra);
        if (isset($extra) && isset($extra['allowNull'])) {
            $this->allowNull = $extra['allowNull'];
        }
    }

    /**
     * Validates Method
     * @param mixed $value to validate
     * @return bool true or false
     */
    public function validate($value): bool {
        $paths = explode(' ', $value);
        if (!$this->allowNull && count($paths) != 2) {
            return false;
        } else if ($this->allowNull && strlen($value) == 0) {
            return true;
        }
        $isDateValid = $this->dateValidationRule->validate($paths[0]);
        $isTimeValid = $this->timeValidationRule->validate($paths[1]);
        return ($isDateValid && $isTimeValid);
    }

}
