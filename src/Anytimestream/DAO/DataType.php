<?php
namespace Anytimestream\DAO;

class DataType {
    const STRING = "string";
    const INTEGER = "integer";
    const DOUBLE = "double";
    const DECIMAL = "decimal";
    const BOOLEAN = "boolean";
    const TINYINT = "tinyint";
    const DATETIME = "datetime";
    const DATE = "date";
    const TIME = "time";
    const TIMESTAMP = "timestamp";
    const MEDIUMTEXT = "mediumtext";
    const JSON = "json";
}

