<?php
namespace tests;

use Anytimestream\DAO\SoftDeletePersistableObject;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class SoftDeletePersistableObjectTest extends TestCase {
    
    public function testColumn() {
        $softDeletePersistableObject = $this->getClass();
        $columns = $softDeletePersistableObject->getColumns();
        
        $this->assertEquals(true, count($columns) == 2);
        
    }
    
    public function testDeleted() {
        $softDeletePersistableObject = $this->getClass();
        
        $this->assertEquals(true, $softDeletePersistableObject->getDeleted() == 0);
        
        $softDeletePersistableObject->setDeleted();
        
        $this->assertEquals(true, $softDeletePersistableObject->getDeleted() == 1);
        
    }
    
    private function getClass(): SoftDeletePersistableObject {
        return new class extends SoftDeletePersistableObject {
            
        };
    }
}
