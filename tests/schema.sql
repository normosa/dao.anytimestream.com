/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
 `id` varchar(45) NOT NULL,
 `name` varchar(30) NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `name`(`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
 `id` varchar(45) NOT NULL,
 `name` varchar(30) NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `name`(`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `role_permissions`;

CREATE TABLE `role_permissions` (
 `id` varchar(45) NOT NULL,
 `role_id` varchar(45) NOT NULL,
 `permission_id` varchar(45) NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 KEY `role_permissions_fk_role_id` (`role_id`),
 CONSTRAINT `role_permissions_fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
 KEY `role_permissions_fk_permission_id` (`permission_id`),
 CONSTRAINT `role_permissions_fk_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `schema_datatype`;

CREATE TABLE `schema_datatype` (
 `id` varchar(45) NOT NULL,
 `name` varchar(200) NOT NULL,
 `status` tinyint(1) NOT NULL,
 `size` double NOT NULL,
 `amount` decimal(10,2) NOT NULL,
 `state` tinyint(2) NOT NULL,
 `body` mediumtext NOT NULL,
 `time` time NOT NULL,
 `date` date NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
 `id` varchar(45) NOT NULL,
 `username` varchar(200) NOT NULL,
 `password` varchar(40) NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 UNIQUE KEY `username`(`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
 `id` varchar(45) NOT NULL,
 `user_id` varchar(45) NOT NULL,
 `firstname` varchar(25) NOT NULL,
 `lastname` varchar(25) NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 KEY `user_profiles_fk_user_id` (`user_id`),
 CONSTRAINT `user_profiles_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
 `id` varchar(45) NOT NULL,
 `user_id` varchar(45) NOT NULL,
 `role_id` varchar(45) NOT NULL,
 `creation_date` datetime NOT NULL,
 `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 KEY `user_roles_fk_user_id` (`user_id`),
 CONSTRAINT `user_roles_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
 KEY `user_roles_fk_role_id` (`role_id`),
 CONSTRAINT `user_roles_fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
