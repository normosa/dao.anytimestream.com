<?php
namespace tests;

use Anytimestream\DAO\Databases\MYSQLSchemaBuilder;

require_once(__DIR__.'/../vendor/autoload.php');

$dirs = array(__DIR__."/Databases/SchemaBuilders");
$mysqlSchemaBuilder = MYSQLSchemaBuilder::CreateBuilder($dirs);
$mysqlSchemaBuilder->build();
$mysqlSchemaBuilder->saveSchema(__DIR__.'/schema.sql');
//echo $mysqlSchemaBuilder->getSchema();


