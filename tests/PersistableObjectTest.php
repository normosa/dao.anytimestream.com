<?php
namespace tests;

use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class PersistableObjectTest extends TestCase {
    
    public function testDataSourceName() {
        $samplePersistableObject = new SamplePersistableObject();
        
        $this->assertEquals(true, strcmp($samplePersistableObject->getDataSourceName(), "users") == 0);
    }
    
    public function testPrimaryKeyColumn() {
        $samplePersistableObject = new SamplePersistableObject();
        
        $this->assertEquals(true, strcmp($samplePersistableObject->getPrimaryKeyColumn(), "id") == 0);
    }
    
    public function testTimestampColumn() {
        $samplePersistableObject = new SamplePersistableObject();
        
        $this->assertEquals(true, strcmp($samplePersistableObject->getTimestampColumn(), "last_changed") == 0);
    }
    
    public function testUniqueColumns() {
        $samplePersistableObject = new SamplePersistableObject();
        $uniqueColumns = $samplePersistableObject->getUniqueColumns();
        
        $this->assertEquals(true, count($uniqueColumns) == 1);
        $this->assertEquals(true, in_array("username", $uniqueColumns[0], true));
    }
    
    public function testForeignKeys() {
        $samplePersistableObject = new SamplePersistableObject();
        $foreignKeys = $samplePersistableObject->getForeignKeys();
        
        $this->assertEquals(true, count($foreignKeys) == 1);
        $this->assertEquals(true, strcmp($foreignKeys[0]->column, "username") == 0);
        $this->assertEquals(true, strcmp($foreignKeys[0]->refTable, "roles") == 0);
        $this->assertEquals(true, strcmp($foreignKeys[0]->refColumn, "id") == 0);
    }
    
    public function testColumnAnnotation() {
        $samplePersistableObject = new SamplePersistableObject();
        $columnAnnotation = $samplePersistableObject->getColumnAnnotation("username");
        
        $this->assertEquals(true, strcmp($columnAnnotation->name, "username") == 0);
        $this->assertEquals(true, $columnAnnotation->isSavable);
    }
    
    public function testColumnNames() {
        $samplePersistableObject = new SamplePersistableObject();
        $columnNames = $samplePersistableObject->getColumnNames(array('id', 'username', 'password', 'creationDate', 'lastChanged'));
        
        $this->assertEquals(true, in_array('creation_date', $columnNames));
        $this->assertEquals(true, in_array('last_changed', $columnNames));
        $this->assertEquals(true, in_array('id', $columnNames));
        $this->assertEquals(true, in_array('username', $columnNames));
        $this->assertEquals(true, in_array('password', $columnNames));
    }
    
    public function testColumnValue() {
        $username = "chat4zeal@yahoo.com";
        
        $samplePersistableObject = new SamplePersistableObject();
        $samplePersistableObject->setUsername($username);
        
        $this->assertEquals(true, strcmp($samplePersistableObject->getColumnValue("username"), $username) == 0);
        $this->assertEquals(true, strcmp($samplePersistableObject->getColumnValue("creation_date"), $samplePersistableObject->getCreationDate()) == 0);
    }
    
    public function testColumns() {
        $samplePersistableObject = new SamplePersistableObject();
        $columns = $samplePersistableObject->getColumns();
        
        $this->assertEquals(true, count($columns) == 5);
    }
    
    public function testGetRowData() {
        $username = "chat4zeal@yahoo.com";
        
        $samplePersistableObject = new SamplePersistableObject();
        $samplePersistableObject->setUsername($username);
        
        $rowData = $samplePersistableObject->getRowData();
        
        $this->assertEquals(true, strcmp($username, $rowData['username']) == 0);
    }
    
    public function testSetRowData() {
        $rowData['creation_date'] = date('Y-m-d H:i:s');
        
        $samplePersistableObject = new SamplePersistableObject();
        $samplePersistableObject->setRowData($rowData);
        
        $this->assertEquals(true, strcmp($samplePersistableObject->getCreationDate(), $rowData['creation_date']) == 0);
    }
    
     public function testProperty() {
        $username = "chat4zeal@yahoo.com";
        
        $samplePersistableObject = new SamplePersistableObject();
        $samplePersistableObject->setProperty("username", $username);
        
        $this->assertEquals(true, strcmp($samplePersistableObject->getUsername(), $username) == 0);
    }
}
