<?php
namespace tests\Validation;

use Anytimestream\DAO\Validation\CanonicalPathValidationRule;
use Anytimestream\DAO\Validation\CommaStringValidationRule;
use Anytimestream\DAO\Validation\DateTimeValidationRule;
use Anytimestream\DAO\Validation\DateValidationRule;
use Anytimestream\DAO\Validation\EmailValidationRule;
use Anytimestream\DAO\Validation\IntegerValidationRule;
use Anytimestream\DAO\Validation\InternationalPhoneValidationRule;
use Anytimestream\DAO\Validation\LocalPhoneValidationRule;
use Anytimestream\DAO\Validation\NotValidValidationRule;
use Anytimestream\DAO\Validation\NumberValidationRule;
use Anytimestream\DAO\Validation\StringValidationRule;
use Anytimestream\DAO\Validation\TimeValidationRule;
use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../../vendor/autoload.php');

class ValidationRulesTest extends TestCase {
    
    public function testEmailValidationRule() {
        $emailValidationRule = new EmailValidationRule();
        $this->assertEquals(true, $emailValidationRule->validate("chat4zeal@yahoo.com"));
        $this->assertEquals(false, $emailValidationRule->validate("chat4zealyahoo.com"));
        
        $extra['allowNull'] = true;
        $emailValidationRule2 = new EmailValidationRule($extra);
        $this->assertEquals(true, $emailValidationRule2->validate("chat4zeal@yahoo.com"));
        $this->assertEquals(false, $emailValidationRule2->validate("chat4zealyahoo"));
        $this->assertEquals(true, $emailValidationRule2->validate(""));
    }
    
    public function testDateValidationRule() {
        $dateValidationRule = new DateValidationRule();
        $this->assertEquals(true, $dateValidationRule->validate("1/1/2009"));
        $this->assertEquals(true, $dateValidationRule->validate("01/03/2012"));
        $this->assertEquals(false, $dateValidationRule->validate("112/12/1200"));
        
        $extra['allowNull'] = true;
        $dateValidationRule2 = new DateValidationRule($extra);
        $this->assertEquals(true, $dateValidationRule2->validate("1/1/2009"));
        $this->assertEquals(false, $dateValidationRule2->validate("112/12/1200"));
        $this->assertEquals(false, $dateValidationRule2->validate("11/13/1200"));
        $this->assertEquals(false, $dateValidationRule2->validate("30/02/2018"));
        $this->assertEquals(true, $dateValidationRule2->validate(""));
    }
    
    public function testCanonicalPathValidationRule() {
        $canonicalPathValidationRule = new CanonicalPathValidationRule();
        $this->assertEquals(true, $canonicalPathValidationRule->validate("-path-hdhdhdd-jkdkkdkd"));
        $this->assertEquals(true, $canonicalPathValidationRule->validate("hhd-fnnff-fnnff"));
        $this->assertEquals(false, $canonicalPathValidationRule->validate(""));
        
        $extra['min'] = 5;
        $extra['max'] = 15;
        $canonicalPathValidationRule2 = new CanonicalPathValidationRule($extra);
        $this->assertEquals(false, $canonicalPathValidationRule2->validate("-path-hdhdhdd-jkdkkdkd"));
        $this->assertEquals(true, $canonicalPathValidationRule2->validate("hhd-fnnff-fnnff"));
        $this->assertEquals(false, $canonicalPathValidationRule2->validate("123"));
    }
    
    public function testCommaStringPathValidationRule() {
        $extra['min'] = 2;
        $extra['max'] = 5;
        $commaStringValidationRule = new CommaStringValidationRule($extra);
        $this->assertEquals(true, $commaStringValidationRule->validate("1,2,3"));
        $this->assertEquals(true, $commaStringValidationRule->validate("1,1,1,1,1"));
        $this->assertEquals(false, $commaStringValidationRule->validate("1"));
        $this->assertEquals(false, $commaStringValidationRule->validate("1,2,3,4,5,6"));
        $this->assertEquals(false, $commaStringValidationRule->validate(""));
    }
    
    public function testTimeValidationRule() {
        $timeValidationRule = new TimeValidationRule();
        $this->assertEquals(true, $timeValidationRule->validate("05:56:29"));
        $this->assertEquals(false, $timeValidationRule->validate("25:56:29"));
        
        $extra['allowNull'] = true;
        $timeValidationRule2 = new TimeValidationRule($extra);
        $this->assertEquals(true, $timeValidationRule2->validate("05:56:29"));
        $this->assertEquals(false, $timeValidationRule2->validate("25:56:29"));
        $this->assertEquals(true, $timeValidationRule2->validate(""));
    }
    
    public function testDatetimeValidationRule() {
        $dateTimeValidationRule = new DateTimeValidationRule();
        $this->assertEquals(true, $dateTimeValidationRule->validate("12/12/2018 05:56:29"));
        $this->assertEquals(false, $dateTimeValidationRule->validate("12/12/2018 25:56:29"));
        
        $extra['allowNull'] = true;
        $dateTimeValidationRule2 = new DateTimeValidationRule($extra);
        $this->assertEquals(true, $dateTimeValidationRule2->validate("12/12/2018 05:56:29"));
        $this->assertEquals(false, $dateTimeValidationRule2->validate("12/12/2018 25:56:29"));
        $this->assertEquals(true, $dateTimeValidationRule2->validate(""));
    }
    
    public function testStringValidationRule() {
        $stringValidationRule = new StringValidationRule();
        $this->assertEquals(true, $stringValidationRule->validate(""));
        
        $extra['min'] = 5;
        $stringValidationRule2 = new StringValidationRule($extra);
        $this->assertEquals(true, $stringValidationRule2->validate("chat4zeal@yahoo.com"));
        $this->assertEquals(false, $stringValidationRule2->validate("1234"));
        
        $extra['max'] = 12;
        $stringValidationRule3 = new StringValidationRule($extra);
        $this->assertEquals(false, $stringValidationRule3->validate("chat4zeal@yahoo.com"));
        $this->assertEquals(true, $stringValidationRule3->validate("12345678"));
    }
    
    public function testIntegerValidationRule() {
        $integerValidationRule = new IntegerValidationRule();
        $this->assertEquals(true, $integerValidationRule->validate(1));
        $this->assertEquals(true, $integerValidationRule->validate(4));
        $this->assertEquals(false, $integerValidationRule->validate(0.5));
        $this->assertEquals(false, $integerValidationRule->validate("1.0"));
        
        $extra['min'] = -2;
        $extra['max'] = 5;
        $integerValidationRule2 = new IntegerValidationRule($extra);
        $this->assertEquals(true, $integerValidationRule2->validate(3));
        $this->assertEquals(true, $integerValidationRule2->validate(-2));
        $this->assertEquals(false, $integerValidationRule2->validate(-3));
        $this->assertEquals(false, $integerValidationRule2->validate(6));
        $this->assertEquals(false, $integerValidationRule2->validate(""));
    }
    
    public function testInternationalPhoneValidationRule() {
        $internationalPhoneValidationRule = new InternationalPhoneValidationRule();
        $this->assertEquals(true, $internationalPhoneValidationRule->validate("2347066081309"));
        $this->assertEquals(true, $internationalPhoneValidationRule->validate(23480170734883));
        $this->assertEquals(false, $internationalPhoneValidationRule->validate(6681309));
        $this->assertEquals(false, $internationalPhoneValidationRule->validate(234706608130918283));
        
        $extra['allowNull'] = true;
        $internationalPhoneValidationRule2 = new InternationalPhoneValidationRule($extra);
        $this->assertEquals(true, $internationalPhoneValidationRule2->validate(2347066081309));
        $this->assertEquals(true, $internationalPhoneValidationRule2->validate(""));
    }
    
    public function testLocalPhoneValidationRule() {
        $localPhoneValidationRule = new LocalPhoneValidationRule();
        $this->assertEquals(true, $localPhoneValidationRule->validate("07066081309"));
        $this->assertEquals(true, $localPhoneValidationRule->validate("08170734883"));
        $this->assertEquals(false, $localPhoneValidationRule->validate(6681309));
        $this->assertEquals(false, $localPhoneValidationRule->validate(234706608130918283));
        
        $extra['allowNull'] = true;
        $localPhoneValidationRule2 = new LocalPhoneValidationRule($extra);
        $this->assertEquals(false, $localPhoneValidationRule2->validate(70660809));
        $this->assertEquals(true, $localPhoneValidationRule2->validate(""));
    }
    
    public function testNotValidValidationRule() {
        $notValidValidationRule = new NotValidValidationRule();
        $this->assertEquals(true, $notValidValidationRule->validate("07066081309"));
        $this->assertEquals(true, $notValidValidationRule->validate("08170734883"));
        $this->assertEquals(false, $notValidValidationRule->validate("-"));
        $this->assertEquals(true, $notValidValidationRule->validate(""));
        
        $extra['notValid'] = "";
        $notValidValidationRule2 = new NotValidValidationRule($extra);
        $this->assertEquals(true, $notValidValidationRule2->validate(70660809));
        $this->assertEquals(false, $notValidValidationRule2->validate(""));
    }
    
    public function testNumberValidationRule() {
        $numberValidationRule = new NumberValidationRule();
        $this->assertEquals(true, $numberValidationRule->validate("0706608"));
        $this->assertEquals(true, $numberValidationRule->validate(10.00));
        $this->assertEquals(false, $numberValidationRule->validate("-"));
        $this->assertEquals(true, $numberValidationRule->validate(0));
        
        $extra['min'] = 3.4;
        $extra['max'] = 7;
        $numberValidationRule2 = new NumberValidationRule($extra);
        $this->assertEquals(true, $numberValidationRule2->validate(5));
        $this->assertEquals(false, $numberValidationRule2->validate(""));
        $this->assertEquals(false, $numberValidationRule2->validate(3.3));
        $this->assertEquals(false, $numberValidationRule2->validate("8"));
    }

}
