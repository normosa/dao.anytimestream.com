<?php
namespace tests;

use Anytimestream\DAO\Databases\Exception\ConnectionException;
use Exception;
use PDO;

class DatabaseUtil {
    
    private $connection;
    private static $instance;
    
    private function __construct(Array $config) {
        try {
            $this->connection = new PDO('mysql:host=' . $config['host'] . ';dbname='.$config['dbname'], $config['username'], $config['password']);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            throw new ConnectionException();
        }
    }
    
    
    public static function GetInstance(Array $config): DatabaseUtil {
        if(self::$instance == null){
            self::$instance = new DatabaseUtil($config);
        }
        return self::$instance;
    }


    /**
     * Begin transaction when applicable
     */
    public function beginTransaction() {
        if (!$this->connection->inTransaction()) {
            $this->connection->beginTransaction();
        }
    }
    
    /**
     * Commit transaction when applicable
     */
    public function commit() {
        if ($this->connection->inTransaction()) {
            $this->connection->commit();
        }
    }

    /**
     * Close connection
     */
    public function close() {
        $this->connection = null;
    }

    /**
     * RollBack Transaction when applicable
     */
    public function rollBack() {
        if ($this->connection->inTransaction()) {
            $this->connection->rollBack();
        }
    }
    
    /**
     * Executes sql to database
     * @param String $sql Query string
     * @param String $query query string
     * @param Array $columnValues values to bind
     * @return Statement result statement
     */
    public function executeStatment($sql, $columnValues) {
        $statement = $this->connection->prepare($sql);
        $statement->execute($columnValues);
        return $statement;
    }
    
}

