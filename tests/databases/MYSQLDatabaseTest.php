<?php

namespace tests\Databases;

use Anytimestream\DAO\Databases\Exception\ObjectDeleteException;
use Anytimestream\DAO\Databases\Exception\ObjectNotFoundException;
use Anytimestream\DAO\Databases\MYSQLDatabase;
use Exception;
use PHPUnit\Framework\TestCase;
use tests\DatabaseUtil;

require_once(__DIR__ . '/../../vendor/autoload.php');

class MYSQLDatabaseTest extends TestCase {

    const HOST = "localhost";
    const USERNAME = "root";
    const PASSWORD = "";
    const DBNAME = "anytimestream_dao";
    const DROP_TABLE = "DROP TABLE IF EXISTS `users`";
    const CREATE_TABLE = "CREATE TABLE `users` (
        `id` varchar(45) NOT NULL,
        `username` varchar(200) NOT NULL,
        `password` varchar(40) NOT NULL,
        `creation_date` datetime NOT NULL,
        `last_changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY `username`(`username`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
    
    const DUMMY_USERS = array(
        array('12345','norman@yahoo.com','welcome', '2018-06-11 10:10:10', '2018-06-11 10:10:11'),
        array('12347','norman_osaruyi@yahoo.com','welcome', '2018-06-11 10:10:10', '2018-06-11 10:10:11'),
        array('145346','norman124@yahoo.com','welcome', '2018-06-11 10:10:10', '2018-06-11 10:10:11')
    );

    public static function SetUpBeforeClass() {
        $databaseUtil = self::GetDatabaseUtil();
        self::DropTable($databaseUtil);
        $databaseUtil->executeStatment(self::CREATE_TABLE, array());
    }

    public static function TearDownAfterClass() {
        $databaseUtil = self::GetDatabaseUtil();
        self::DropTable($databaseUtil);
    }
    
    protected function setUp(){
        $this->createDummies();
    }
    
    protected function tearDown(){
        $this->removeDummies();
    }

    public function testInsert() {
        $mysqlDatabase = self::GetMYSQLDatabase();
        $user = new User();
        $user->setUsername("chat4zeal@yahoo.com");
        $user->setPassword("welcome");
        $mysqlDatabase->save($user);
        
        $databaseUtil = self::GetDatabaseUtil();
        $statement = $databaseUtil->executeStatment('select * from users where username = ?', array($user->getUsername()));
        
        if(!($row = $statement->fetch())){
            $this->assertEquals(true, false);
        }
        
        $this->assertEquals(true, strcmp($user->getId(), $row['id']) == 0);
        $this->assertEquals(true, strcmp($user->getUsername(), $row['username']) == 0);
        $this->assertEquals(true, strcmp($user->getPassword(), $row['password']) == 0);
        $this->assertEquals(true, strcmp($user->getCreationDate(), $row['creation_date']) == 0);
    }
    
    public function testUpdate() {
        $databaseUtil = self::GetDatabaseUtil();
        $statement = $databaseUtil->executeStatment('select * from users where username = ?', array("norman@yahoo.com"));
        
        if(!($row = $statement->fetch())){
            $this->assertEquals(true, false);
        }
        
        $mysqlDatabase = self::GetMYSQLDatabase();
        
        $user = new User();
        $user->setRowData($row);
        
        $user->setUsername("chatwithdgenius@yahoo.com");
        $user->setPassword("adminadmin@1@1");
        
        $mysqlDatabase->save($user);
        
        $statement2 = $databaseUtil->executeStatment('select * from users where username = ?', array($user->getUsername()));
        
        if(!($row2 = $statement2->fetch())){
            $this->assertEquals(true, false);
        }
        
        $this->assertEquals(true, strcmp($user->getUsername(), $row2['username']) == 0);
        $this->assertEquals(true, strcmp($user->getPassword(), $row2['password']) == 0);
    }
    
    public function testFindById() {
        $mysqlDatabase = self::GetMYSQLDatabase();
        
        $user = $mysqlDatabase->findById(User::class, "12345");
        
        $this->assertEquals(true, strcmp($user->getId(), self::DUMMY_USERS[0][0]) == 0);
        $this->assertEquals(true, strcmp($user->getUsername(), self::DUMMY_USERS[0][1]) == 0);
        $this->assertEquals(true, strcmp($user->getPassword(), self::DUMMY_USERS[0][2]) == 0);
        $this->assertEquals(true, strcmp($user->getCreationDate(), self::DUMMY_USERS[0][3]) == 0);
    }
    
    public function testFind() {
        $mysqlDatabase = self::GetMYSQLDatabase();
        
        try {
            $mysqlDatabase->find(User::class, array('username' => 'test@yahoo.com'));
            $this->assertEquals(true, false);
        } catch (ObjectNotFoundException $ex) {
            $this->assertEquals(true, true);
        }
        
        $user = $mysqlDatabase->find(User::class, array('username' => 'norman@yahoo.com'));
        
        $this->assertEquals(true, strcmp($user->getId(), self::DUMMY_USERS[0][0]) == 0);
        $this->assertEquals(true, strcmp($user->getUsername(), self::DUMMY_USERS[0][1]) == 0);
        $this->assertEquals(true, strcmp($user->getPassword(), self::DUMMY_USERS[0][2]) == 0);
        $this->assertEquals(true, strcmp($user->getCreationDate(), self::DUMMY_USERS[0][3]) == 0);
    }
    
    public function testFindAll() {
        $mysqlDatabase = self::GetMYSQLDatabase();
        $users = $mysqlDatabase->findAll(User::class);
        
        $this->assertEquals(true, (count($users) == 3));
        $this->assertEquals(true, strcmp("12347", self::DUMMY_USERS[1][0]) == 0);
        
        $users2 = $mysqlDatabase->findAll(User::class, array('username' => 'norman@yahoo.com'));
        
        $this->assertEquals(true, (count($users2) == 1));
        $this->assertEquals(true, strcmp("12345", self::DUMMY_USERS[0][0]) == 0);
    }
    
    public function testDeleteById() {
        $mysqlDatabase = self::GetMYSQLDatabase();
        $result = $mysqlDatabase->deleteById(User::class, "12345");
        
        $this->assertEquals(true, $result);
    }
    
    public function testDelete() {
        $mysqlDatabase = self::GetMYSQLDatabase();
        $result = $mysqlDatabase->delete(User::class, array('username' => 'norman@yahoo.com'));
        
        $this->assertEquals(true, $result);
        
        try {
            $mysqlDatabase->delete(User::class, array('username' => 'test@yahoo.com'));
            $this->assertEquals(true, false);
        } catch (ObjectDeleteException $ex) {
            $this->assertEquals(true, true);
        }
    }

    private static function GetDatabaseUtil() {
        $config['host'] = self::HOST;
        $config['username'] = self::USERNAME;
        $config['password'] = self::PASSWORD;
        $config['dbname'] = self::DBNAME;

        return DatabaseUtil::GetInstance($config);
    }
    
    private static function DropTable(DatabaseUtil $databaseUtil){
        $databaseUtil->executeStatment(self::DROP_TABLE, array());
    }
    
    private function createDummies(){
        $databaseUtil = self::GetDatabaseUtil();
        
        $rowCount = 0;
        
        for($i = 0; $i < count(self::DUMMY_USERS); $i++){
            $statement = $databaseUtil->executeStatment('insert into users values(?,?,?,?,?)', self::DUMMY_USERS[$i]);
            $rowCount += $statement->rowCount();
        }
        
        if($rowCount != count(self::DUMMY_USERS)){
            throw new Exception("error creating dummy users");
        }
    }
    
    private function removeDummies(){
        $databaseUtil = self::GetDatabaseUtil();
        
        $databaseUtil->executeStatment('delete from users', array());
    }
    
    private static function GetMYSQLDatabase() {
        $config['host'] = self::HOST;
        $config['username'] = self::USERNAME;
        $config['password'] = self::PASSWORD;
        $config['dbname'] = self::DBNAME;

        $mysqlDatabase = MYSQLDatabase::GetInstance($config);
        return $mysqlDatabase;
    }
  

}
