<?php

namespace tests\Databases\SchemaBuilders;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\ForeignKey;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\DataType;

/**
 * @Table(name="user_roles")
 * @PrimaryKey(column="id")
 * @ForeignKey(column="user_id", refTable="users", refColumn="id")
 * @ForeignKey(column="role_id", refTable="roles", refColumn="id")  
 * @Timestamp(column="last_changed")
 */
class UserRole extends PersistableObject {

    /**
     * @Column(name="id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $id;

    /**
     * @Column(name="user_id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $userId;

    /**
     * @Column(name="role_id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $roleId;

    /**
     * @Column(name="creation_date", dataType=DataType::DATETIME)
     */
    private $creationDate;

    /**
     * @Column(name="last_changed", dataType=DataType::TIMESTAMP, isSavable=false)
     */
    private $lastChanged;

    public function __construct() {
        parent::__construct();
        $this->id = str_replace(".", "", uniqid(rand(), true));
        $this->creationDate = date('Y-m-d H:i:s');
    }

    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->userId;
    }
    
    public function setUserId(string $value){
        $this->setProperty("userId", $value);
    }

    public function getRoleId() {
        return $this->roleId;
    }
    
    public function setRoleId(string $value){
        $this->setProperty("roleId", $value);
    }

    public function getCreationDate() {
        return $this->creationDate;
    }

    public function getLastChanged() {
        return $this->lastChanged;
    }

}
