<?php

namespace tests\Databases\SchemaBuilders;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\ForeignKey;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\DataType;

/**
 * @Table(name="role_permissions")
 * @PrimaryKey(column="id")
 * @ForeignKey(column="role_id", refTable="roles", refColumn="id")
 * @ForeignKey(column="permission_id", refTable="permissions", refColumn="id")
 * @Timestamp(column="last_changed")
 */
class RolePermission extends PersistableObject {

    /**
     * @Column(name="id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $id;
    
    /**
     * @Column(name="role_id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $roleId;
    
    /**
     * @Column(name="permission_id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $permissionId;

    /**
     * @Column(name="creation_date", dataType=DataType::DATETIME)
     */
    private $creationDate;

    /**
     * @Column(name="last_changed", dataType=DataType::TIMESTAMP, isSavable=false)
     */
    private $lastChanged;

    public function __construct() {
        parent::__construct();
        $this->id = str_replace(".", "", uniqid(rand(), true));
        $this->creationDate = date('Y-m-d H:i:s');
    }

    public function getId() {
        return $this->id;
    }

    public function getRoleId() {
        return $this->roleId;
    }
    
    public function setRoleId(string $value){
        $this->setProperty("roleId", $value);
    }
    
    public function getPermissionId() {
        return $this->permissionId;
    }
    
    public function setPermissionId(string $value){
        $this->setProperty("permissionId", $value);
    }

    public function getCreationDate() {
        return $this->creationDate;
    }

    public function getLastChanged() {
        return $this->lastChanged;
    }

}
