<?php
namespace tests\Databases\SchemaBuilders;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\ForeignKey;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\DataType;

/** 
 * @Table(name="users")
 * @PrimaryKey(column="id")
 * @Index(type="Unique", columns={"username"})
 * @Timestamp(column="last_changed")
 */
class User extends PersistableObject{
    
    /**
     * @Column(name="id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $id;
    
    /**
     * @Column(name="username", dataType=DataType::STRING, extra={"max"=200})
     * @Validator(validationRule="Anytimestream\DAO\Validation\EmailValidationRule", errorCode="username", extra={})
     */
    private $username;
    
    /**
     * @Column(name="password", dataType=DataType::STRING, extra={"max"=40})
     * @Validator(validationRule="Anytimestream\DAO\Validation\StringValidationRule", errorCode="password", extra={"min"=6,"max"=40})
     */
    private $password;
    
    /**
     * @Column(name="creation_date", dataType=DataType::DATETIME)
     */
    private $creationDate;
    
    /**
     * @Column(name="last_changed", dataType=DataType::TIMESTAMP, isSavable=false)
     */
    private $lastChanged;
    
    public function __construct() {
        parent::__construct();
        $this->id = str_replace(".", "", uniqid(rand(), true));
        $this->creationDate = date('Y-m-d H:i:s');
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUsername(){
        return $this->username;
    }
    
    public function setUsername($value){
        $this->setProperty("username", $value);
    }
    
    public function getPassword(){
        return $this->password;
    }
    
    public function setPassword($value){
        $this->setProperty("password", $value);
    }
    
    public function getCreationDate(){
        return $this->creationDate;
    }
    
    public function getLastChanged(){
        return $this->lastChanged;
    }
}

