<?php
namespace tests\Databases\SchemaBuilders;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\ForeignKey;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\DataType;

/** 
 * @Table(name="user_profiles")
 * @PrimaryKey(column="id")
 * @ForeignKey(column="user_id", refTable="users", refColumn="id")  
 * @Timestamp(column="last_changed")
 */
class UserProfile extends PersistableObject{
    
    /**
     * @Column(name="id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $id;
    
    /**
     * @Column(name="user_id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $userId;
    
    /**
     * @Column(name="firstname", dataType=DataType::STRING, extra={"max"=25})
     * @Validator(validationRule="Anytimestream\DAO\Validation\StringValidationRule", errorCode="name", extra={"min"=3,"max"=25})
     */
    private $firstname;
    
    /**
     * @Column(name="lastname", dataType=DataType::STRING, extra={"max"=25})
     * @Validator(validationRule="Anytimestream\DAO\Validation\StringValidationRule", errorCode="name", extra={"min"=3,"max"=25})
     */
    private $lastname;
    
    /**
     * @Column(name="creation_date", dataType=DataType::DATETIME)
     */
    private $creationDate;
    
    /**
     * @Column(name="last_changed", dataType=DataType::TIMESTAMP, isSavable=false)
     */
    private $lastChanged;
    
    public function __construct() {
        parent::__construct();
        $this->id = str_replace(".", "", uniqid(rand(), true));
        $this->creationDate = date('Y-m-d H:i:s');
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUserId(){
        return $this->userId;
    }
    
    public function getFirstname(){
        return $this->firstname;
    }
    
    public function setFirstname(string $value){
        $this->setProperty("firstname", $value);
    }
    
    public function getLastname(){
        return $this->lastname;
    }
    
    public function setLastname(string $value){
        $this->setProperty("lastname", $value);
    }
    
    public function getCreationDate(){
        return $this->creationDate;
    }
    
    public function getLastChanged(){
        return $this->lastChanged;
    }
}

