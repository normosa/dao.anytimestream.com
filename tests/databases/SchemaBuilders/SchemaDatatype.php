<?php
namespace tests\Databases\SchemaBuilders;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\ForeignKey;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\DataType;

/** 
 * @Table(name="schema_datatype")
 * @PrimaryKey(column="id")
 * @Timestamp(column="last_changed")
 */
class SchemaDatatype extends PersistableObject{
    
    /**
     * @Column(name="id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $id;
    
    /**
     * @Column(name="name", dataType=DataType::STRING, extra={"max"=200})
     */
    private $name;
    
    /**
     * @Column(name="status", dataType=DataType::BOOLEAN)
     */
    private $status;
    
    /**
     * @Column(name="size", dataType=DataType::DOUBLE)
     */
    private $size;
    
     /**
     * @Column(name="amount", dataType=DataType::DECIMAL, extra={"max"="10,2"})
     */
    private $amount;
    
    /**
     * @Column(name="state", dataType=DataType::TINYINT, extra={"max"=2})
     */
    private $state;
    
    /**
     * @Column(name="body", dataType=DataType::MEDIUMTEXT)
     */
    private $body;
    
    /**
     * @Column(name="time", dataType=DataType::TIME)
     */
    private $time;
    
    /**
     * @Column(name="date", dataType=DataType::DATE)
     */
    private $date;
    
    /**
     * @Column(name="creation_date", dataType=DataType::DATETIME)
     */
    private $creationDate;
    
    /**
     * @Column(name="last_changed", dataType=DataType::TIMESTAMP, isSavable=false)
     */
    private $lastChanged;
}

