<?php
namespace tests\Databases\SchemaBuilders;

use Anytimestream\DAO\Annotations\Column;
use Anytimestream\DAO\Annotations\Table;
use Anytimestream\DAO\Annotations\PrimaryKey;
use Anytimestream\DAO\Annotations\Timestamp;
use Anytimestream\DAO\Annotations\Validator;
use Anytimestream\DAO\Annotations\Index;
use Anytimestream\DAO\PersistableObject;
use Anytimestream\DAO\DataType;

/** 
 * @Table(name="roles")
 * @PrimaryKey(column="id")
 * @Index(type="Unique", columns={"name"})
 * @Timestamp(column="last_changed")
 */
class Role extends PersistableObject{
    
    /**
     * @Column(name="id", dataType=DataType::STRING, extra={"max"=45})
     */
    private $id;
    
    /**
     * @Column(name="name", dataType=DataType::STRING, extra={"max"=30})
     * @Validator(validationRule="Anytimestream\DAO\Validation\StringValidationRule", errorCode="name", extra={"min"=3,"max"=30})
     */
    private $name;
    
    /**
     * @Column(name="creation_date", dataType=DataType::DATETIME)
     */
    private $creationDate;
    
    /**
     * @Column(name="last_changed", dataType=DataType::TIMESTAMP, isSavable=false)
     */
    private $lastChanged;
    
    public function __construct() {
        parent::__construct();
        $this->id = str_replace(".", "", uniqid(rand(), true));
        $this->creationDate = date('Y-m-d H:i:s');
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setName(string $value){
        $this->setProperty("name", $value);
    }
    
    public function getCreationDate(){
        return $this->creationDate;
    }
    
    public function getLastChanged(){
        return $this->lastChanged;
    }
}

