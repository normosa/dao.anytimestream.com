<?php

namespace tests\Databases;

use Anytimestream\DAO\Databases\Database;
use Anytimestream\DAO\Databases\DatabaseManager;
use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../../vendor/autoload.php');

define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "anytimestream_dao");

class DatabaseManagerTest extends TestCase {
    
    public function testGetDatabase() {
        $database = DatabaseManager::GetDatabase();
        
        $this->assertEquals(true, $database instanceof Database);
    }
}
